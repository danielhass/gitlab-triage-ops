# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/resources/merge_request'
require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/security_branch'

RSpec.describe Triage::PipelineFailure::Config::SecurityBranch do
  let(:project_path_with_namespace) { 'gitlab-org/security/gitlab' }
  let(:source) { 'merge_request_event' }
  let(:event_actor) { 'gitlab-release-tools-bot' }
  let(:merge_request) { Triage::MergeRequest.new(target_branch: 'master') }
  let(:source_job_id) { nil }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      source: source,
      merge_request: merge_request,
      event_actor: event_actor,
      source_job_id: source_job_id)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when project_path_with_namespace is not "gitlab-org/security/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source is not "merge_request_event"' do
      let(:source) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when merge_request target_branch is not "master"' do
      let(:merge_request) { Triage::MergeRequest.new(target_branch: 'foo') }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when event_actor is not "gitlab-release-tools-bot"' do
      let(:event_actor) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source_job_id is present' do
      let(:source_job_id) { '42' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#slack_channel' do
    it 'returns expected channel' do
      expect(subject.slack_channel).to eq('f_upcoming_release')
    end
  end

  describe '#slack_username' do
    it 'returns expected username' do
      expect(subject.slack_username).to eq('GitLab Release Tools Bot')
    end
  end

  describe '#slack_icon' do
    it 'returns expected icon' do
      expect(subject.slack_icon).to eq('ci_failing')
    end
  end
end
