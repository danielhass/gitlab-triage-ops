# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/slack_notifier'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

RSpec.describe Triage::PipelineFailure::SlackNotifier do
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { 'master' }
  let(:variables) do
    [
      { 'key' => 'SCHEDULE_TYPE', 'value' => 'nightly' }
    ]
  end

  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_path_with_namespace: project_path_with_namespace,
      project_id: 999,
      ref: ref,
      sha: 'sha',
      source_job_id: nil,
      web_url: 'web_url',
      commit_header: 'commit_header',
      event_actor: Triage::User.new(name: 'John Doe', username: 'john'),
      source: 'schedule',
      variables: variables,
      created_at: Time.now,
      merge_request: Triage::MergeRequest.new(title: 'Hello'),
      project_web_url: "https://gitlab.test/#{project_path_with_namespace}")
  end

  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:failed_jobs) do
    [
      double(name: 'foo', web_url: 'foo_web_url', allow_failure: false),
      double(name: 'bar', web_url: 'foo_web_url', allow_failure: false),
      double(name: 'baz', web_url: 'baz_web_url', allow_failure: false)
    ]
  end

  let(:incident_double) { double('Incident', iid: 4567, web_url: 'incident_web_url', failed_jobs: failed_jobs) }
  let(:slack_client_double) { double('Slack::Messenger') }
  let(:pipeline_failure_config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

  subject { described_class.new(event: event, config: config, failed_jobs: failed_jobs, slack_webhook_url: 'slack_webhook_url', incident: incident_double) }

  around do |example|
    Timecop.freeze(Time.utc(2020, 3, 31, 8)) { example.run }
  end

  describe '#execute' do
    before do
      allow(Slack::Messenger).to receive(:new).and_return(slack_client_double)
    end

    shared_examples 'Slack message posting' do
      it 'posts a Slack message' do
        expect(slack_client_double).to receive(:ping)
          .with(
            username: described_class::MESSAGE_AUTHOR,
            icon_emoji: described_class::MESSAGE_ICON_EMOJI,
            **JSON.parse(format(config.slack_payload_template, subject.__send__(:template_variables)))
          )

        subject.execute
      end
    end

    it_behaves_like 'Slack message posting'

    context 'without an incident' do
      let(:incident_double) { nil }

      it_behaves_like 'Slack message posting'
    end

    context 'when variables do not include SCHEDULE_TYPE' do
      let(:variables) do
        [
          { 'key' => 'key', 'value' => 'value' }
        ]
      end

      it_behaves_like 'Slack message posting'
    end
  end
end
