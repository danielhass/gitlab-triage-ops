# frozen_string_literal: true

RSpec.shared_context 'with data files stubbed' do
  before do
    stub_request(:get, "https://about.gitlab.com/sections.json")
      .to_return(status: 200, body: read_fixture('sections.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/stages.json")
      .to_return(status: 200, body: read_fixture('stages.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/groups.json")
      .to_return(status: 200, body: read_fixture('groups.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/categories.json")
      .to_return(status: 200, body: read_fixture('categories.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/company/team/team.yml")
      .to_return(status: 200, body: read_fixture('team.yml'), headers: {})

    stub_request(:get, "https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json")
      .to_return(status: 200, body: read_fixture('roulette.json'), headers: {})

    stub_request(:get, "https://gitlab.com/gitlab-org/distribution/monitoring/-/raw/master/lib/data_sources/projects.yaml")
      .to_return(status: 200, body: read_fixture('distribution_projects.yml'), headers: {})
  end
end
