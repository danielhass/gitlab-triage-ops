# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_ux'

RSpec.describe Triage::AutomatedReviewRequestUx do
  include_context 'with slack posting context'
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        wip?: false,
        url: url,
        title: title
      }
    end

    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'Merge request title' }
    let(:label_names) { [Labels::UX_LABEL] }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.update"]
  include_examples 'processor slack options', '#ux-community-contributions'

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when UX is not set' do
      let(:label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when event is a Draft' do
      before do
        allow(event).to receive(:wip?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when docs review was already requested' do
      before do
        allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(messenger_stub).to receive(:ping)
    end

    shared_examples 'process UX merge request' do
      it 'posts a comment' do
        body = add_automation_suffix('processor/community/automated_review_request_ux.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Thanks for helping us improve the UX of GitLab. Your contribution is appreciated! We have pinged our UX team, so stay tuned for their feedback.
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    it_behaves_like 'process UX merge request'
    it_behaves_like 'slack message posting' do
      before do
        allow(subject).to receive(:post_ux_comment)
      end

      let(:message_body) do
        <<~MARKDOWN
          Hi UX team, a new community contribution (#{title}) requires a UX review: #{url}.
        MARKDOWN
      end
    end
  end
end
