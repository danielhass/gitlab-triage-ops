stages:
  - test
  - dry-run
  - schedules-sync
  - single-run
  - pre-hygiene
  - hygiene
  - report
  - close-reports
  - one-off
  - build
  - secrets
  - deploy
  - health-check

workflow:
  name: '$PIPELINE_NAME'
  rules:
    - if: $TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS
      variables:
        PIPELINE_NAME: 'Label community contributions'

    - if: $TRIAGE_GITLAB_FOSS_MIGRATE_ISSUES_TO_GITLAB
      variables:
        PIPELINE_NAME: 'Migrate gitlab-foss issues to gitlab'

    - if: $TRIAGE_UNTRIAGED_COMMUNITY_MERGE_REQUESTS_REPORT
      variables:
        PIPELINE_NAME: 'Generate untriaged community MRs report'

    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
      variables:
        PIPELINE_NAME: '$CI_MERGE_REQUEST_EVENT_TYPE MR pipeline'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For dry-running a schedule with `dry-run:schedule`
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0:bundler-2.3
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile.lock
      prefix: "ruby-gems"
    paths:
      - vendor/ruby/

variables:
  BUNDLE_PATH__SYSTEM: 'false'
  BUNDLE_WITHOUT: "production:development"
  BUNDLE_INSTALL_FLAGS: "--jobs=$(nproc) --retry=3 --quiet"
  TRIAGE_WEB_IMAGE_PATH: "${CI_REGISTRY}/${CI_PROJECT_PATH}/web:${CI_COMMIT_REF_SLUG}"
  DOCKER_VERSION: "20.10.1"
  SIMPLECOV: "true"
  JUNIT_RESULT_FILE: "rspec/junit_rspec.xml"
  COVERAGE_FILE: "coverage/coverage.xml"

include:
  - local: .gitlab/ci/*.yml
  - local: .gitlab/ci/generated/*.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/danger-review.yml'

danger-review:
  before_script: !reference [.ruby-before_script, before_script]
  variables:
    BUNDLE_WITHOUT: "production:development:test"
    BUNDLE_WITH: "danger"
