# frozen_string_literal: true

require_relative '../lib/missed_resource_helper'

Gitlab::Triage::Resource::Context.include MissedResourceHelper
