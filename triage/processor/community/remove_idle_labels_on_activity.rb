# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class RemoveIdleLabelOnActivity < CommunityProcessor
    react_to 'merge_request.update', 'merge_request.note'

    def applicable?
      wider_community_contribution? &&
        idle_or_stale? &&
        (event.by_resource_author? || event.by_team_member?) &&
        # Either the event is a note event, or it's a revision update
        (!event.respond_to?(:revision_update?) || event.revision_update?)
    end

    def process
      comment = <<~COMMENT.chomp
        /unlabel ~"#{Labels::IDLE_LABEL}" ~"#{Labels::STALE_LABEL}"
      COMMENT
      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor removes ~"idle" and ~"stale" labels from community merge requests following a new activity.
      TEXT
    end

    private

    def idle_or_stale?
      (event.label_names & [Labels::IDLE_LABEL, Labels::STALE_LABEL]).any?
    end
  end
end
