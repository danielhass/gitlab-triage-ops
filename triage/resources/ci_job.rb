# frozen_string_literal: true

module Triage
  CiJob = Struct.new(:project_id, :job_id) do
    def retry
      Triage.api_client.job_retry(project_id, job_id).to_h
    end

    def trace
      @trace ||= Triage.api_client.job_trace(project_id, job_id)
    end
  end
end
