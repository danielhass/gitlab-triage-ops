# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class GitalyUpdateBranch < Base
        SLACK_CHANNEL = 'g_gitaly'

        def self.match?(event)
          event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.source == 'merge_request_event' &&
            event.ref == 'release-tools/update-gitaly' &&
            event.source_job_id.nil?
        end

        def slack_channel
          SLACK_CHANNEL
        end
      end
    end
  end
end
